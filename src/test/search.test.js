import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';
import { Search } from '../components/search/search';

const onChange = jest.fn();

describe('search component', () => {
  it('search render ', () => {
    render(<Search postQuery={'search'} setSearchParams={''} onChange={onChange} value="" />);
    expect(screen.getByPlaceholderText(/Search for the course/i)).toBeInTheDocument();
  });

  it('placeholder ', () => {
    render(<Search postQuery="search" setSearchParams="" onChange={onChange} value="" placeholder="search" />);
    expect(screen.getByPlaceholderText(/search/i)).toBeInTheDocument();
  });

  it('form render ', () => {
    render(<Search />);
    expect(screen.queryByRole('search')).toBeNull();
  });
});
