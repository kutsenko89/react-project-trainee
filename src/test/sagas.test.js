import { takeEvery, call } from 'redux-saga/effects';
import * as api from '../api/resetPassword';
import { resetPasswordWatcher } from '../store/sagas/resetPasswordSaga';
import { RESET_PASSWORD, RESET_PASSWORD_DENIED, RESET_PASSWORD_SUCCESS } from '../store/actions/resetPassword/resetPasswordActionTypes';
import { errorResetPasswordAction, resetPasswordAction } from '../store/actions/resetPassword/resetPasswordActions';
import { resetPasswordSaga } from '../store/sagas/resetPasswordSaga';
import { runSaga } from 'redux-saga';

describe('test saga', () => {
  const g = resetPasswordWatcher();

  it('fetches data', () => {
    expect(g.next().value).toEqual(takeEvery(RESET_PASSWORD, resetPasswordSaga));
  });

  it('should be done', () => {
    expect(g.next().done).toBeTruthy();
  });
});

describe('test saga api response', () => {
  const payload = { payload: 'my3@mail.com' };
  const response = {
    data: {},
    hasError: false,
  };
  const g = resetPasswordSaga({ payload });

  it('fetch data', () => {
    expect(g.next().value).toEqual(call(api.resetPassword, payload));
  });
});



describe('fetch data', () => {
  const email = 'my3@mail.com';
  const payload = { payload: email };

  afterAll(() => jest.clearAllMocks());

  it('fetch data without error & push data to store', async () => {
    const  data  = {
        message: '',
        status: 200,
    };

    api.resetPassword = jest.fn().mockResolvedValue(
      data,
    );

    const dispatched = [];

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ state: 'test' }),
      },
      resetPasswordSaga,
      payload
    ).toPromise();

    expect(api.resetPassword).toHaveBeenCalledTimes(1);
    expect(api.resetPassword).toHaveBeenCalledWith(email);

    expect(dispatched).toEqual([resetPasswordAction({data,hasError:false})]);

    api.resetPassword.mockClear();
  });
  
  it('fetch data with error & push data to store', async () => {
    const data = {
        message: 'error',
        status: 400,
    };
    api.resetPassword = jest.fn().mockRejectedValue(data);


    const dispatched = [];

    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({ state: 'test' }),
      },
      resetPasswordSaga,
      payload
    ).toPromise();

    expect(api.resetPassword).toHaveBeenCalledTimes(1);
    expect(api.resetPassword).toHaveBeenCalledWith(email);

    expect(dispatched).toEqual([errorResetPasswordAction({data,hasError:true})]);

    api.resetPassword.mockClear();
  });
});
