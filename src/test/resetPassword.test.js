import { render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { ResetPassword } from '../pages/resetPassword/resetPassword';
import '@testing-library/jest-dom/extend-expect'; // нужно добавить эту строку
import user from '@testing-library/user-event';

const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));

describe('form component', () => {
  const onSubmit = jest.fn();

  beforeEach(() => {
    onSubmit.mockClear();
    render(<ResetPassword onSubmit={onSubmit} />);
  });

  it('form render ',  () => {
    expect(screen.queryByRole('textbox')).toBeInTheDocument();

    expect(screen.getByText(/Email/i)).toBeInTheDocument();

    const clearField =  screen.queryByRole('textbox',{name:/email/i});
    user.type(clearField, 'my32@mail.coms');
    user.click( screen.getByRole('button', { name:  /password/i }));


    const newField =  screen.queryByRole('textbox',{name:/email/i});
    user.type(newField, 'my3@mailcom');
    user.click( screen.getByRole('button', { name:  /password/i }));


  });
});
