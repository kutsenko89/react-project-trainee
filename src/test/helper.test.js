import { roundRating } from '../services/serviceHelpers';

describe('MainPage_utils', () => {
  test('filterCoursesByStudy', () => {
    expect(roundRating(4.23)).toEqual(4.2);
  });
});
