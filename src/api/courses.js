import { axiosFetch } from './api.config';

export const courses = async () => {
  return await axiosFetch
    .get('/courses')
    .then(({ data }) => {
      return {
        hasError: false,
        ...data,
      };
    })
    .catch(({ response: { data } }) => ({
      hasError: true,
      data,
    }));
};

export const getUserCourses = async () => {
  return await axiosFetch
    .get('/study-processes')
    .then(({ data }) => ({
      hasError: false,
      ...data,
    }))
    .catch(({ response: { data } }) => ({
      hasError: true,
      data,
    }));
};
