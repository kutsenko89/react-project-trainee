import { axiosFetch } from './api.config';

export const studyProcess = async payload => {
  return await axiosFetch
    .get(`study-processes/${payload}`)
    .then(({ data }) => ({
      hasError: false,
      ...data,
    }))
    .catch(({ response: { data } }) => ({
      hasError: true,
      data,
    }));
};
