import { axiosFetch } from './api.config';

export const resetPassword = async payload => {
  return await axiosFetch
    .post('/authorization/reset-password', payload)
    .then(({ data }) => data)
    .catch(({ response: { data } }) => data);
};
