import { createSelector } from 'reselect';

export const coursesSelector = coursePath => {
  const courses = store => store?.courses?.[coursePath];
  const hasError = store => store?.courses?.hasError;

  return createSelector(courses, hasError, (courses, hasError) => {
    return {
      courses,
      hasError,
    };
  });
};
