import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { CardList } from '../../../components/studyCard/cardList';
import { coursesSelector } from '../coursesSelect';
import { CoursesWrapper } from '../coursesStyle';

export const ArchivePage = () => {
  const [coursesStatus, setCoursesStatus] = useState(false);
  const { courses, hasError } = useSelector(coursesSelector('archiveCourses'));

  useEffect(() => {
    if (!hasError) {
      setCoursesStatus(true);
    }
  }, [hasError]);

  return <CoursesWrapper>{coursesStatus && <CardList courses={courses} />}</CoursesWrapper>;
};
