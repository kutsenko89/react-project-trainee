import styled from 'styled-components';
import { theme } from '../../../theme/theme';

export const CourseProcessWrapper = styled.div`
  width: 100%;
  height: 100%;
  background: ${theme.colors.lightGray};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  && .course-process-container {
    margin-top: 272px;
    display: flex;
    flex-direction: column;
    align-items: center;
    overflow: auto;
    width: 100%;
    height: 100%;
  }
`;
