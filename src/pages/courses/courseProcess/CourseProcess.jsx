import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { CourseProcessWrapper } from './CourseProcessStyle';
import { useDispatch, useSelector } from 'react-redux';
import { getStudyProcessId } from '../../../store/actions/studyProcess/studyProcessActions';
import { CourseProcessHeader } from '../../../components/coursesProcess/coursesProcessHeader/courseProcessHeader';
import { CoursesProcessContent } from '../../../components/coursesProcess/coursesProcessContent/coursesProcessContent';
import { clearStudyProcessStore } from '../../../store/actions/studyProcess/studyProcessActions';
import { SpinnerLoader } from '../../../components/loader/spinner/spinnerLoader';

export const CourseProcess = () => {
  const [studyProcessDone, setStudyProcessDone] = useState(false);
  const courseId = useLocation().pathname.split(':')[1];
  const studyProcess = useSelector(store => store?.studyProcess);
  const dispatch = useDispatch();
  const readyCourses = useSelector(store => store?.courses);
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    if (!readyCourses.hasError) {
      dispatch(getStudyProcessId(courseId));
    }

    setLoader(true);

    return () => dispatch(clearStudyProcessStore());
  }, [courseId, readyCourses]);

  useEffect(() => {
    if (!studyProcess?.hasError) {
      setStudyProcessDone(true);
    }
  }, [studyProcess]);

  useEffect(() => {
    if (studyProcessDone) {
      setTimeout(() => setLoader(false), 700);
    }
  }, [studyProcessDone]);

  const renderHeaderCoursesProcess = studyProcess => {
    return (
      <>
        <CourseProcessHeader
          name={studyProcess?.courseName}
          technologies={studyProcess?.technologies.map(item => item?.name || '')}
          ranked={studyProcess?.status.averageMark}
          mentor={studyProcess?.admins}
        />
      </>
    );
  };

  return (
    <CourseProcessWrapper>
      <div className="course-process-container">
        {studyProcessDone && (
          <>
            {renderHeaderCoursesProcess(studyProcess?.studyProcess)}
            <CoursesProcessContent studyProcess={studyProcess?.studyProcess} />
          </>
        )}
      </div>
      {loader && <SpinnerLoader responseStatus={studyProcessDone} />}
    </CourseProcessWrapper>
  );
};
