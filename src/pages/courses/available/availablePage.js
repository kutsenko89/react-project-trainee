import React from 'react';
import { CoursesWrapper } from '../coursesStyle';
import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { CardList } from '../../../components/studyCard/cardList';
import { coursesSelector } from '../coursesSelect';

export const AvailablePage = () => {
  const [coursesStatus, setCoursesStatus] = useState(false);
  const { courses, hasError } = useSelector(coursesSelector('availableCourses'));

  useEffect(() => {
    if (!hasError) {
      setCoursesStatus(true);
    }
  }, [hasError]);

  return <CoursesWrapper>{coursesStatus && <CardList courses={courses} />}</CoursesWrapper>;
};
