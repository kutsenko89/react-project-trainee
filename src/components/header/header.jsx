import React, { useEffect, useState } from 'react';
import { HeaderWrapper } from './headerStyle';
import Logo from './../../../public/svg/Vector.svg';
import { Link } from 'react-router-dom';
import { SignOut } from '../signOut/signOut';

const Header = () => {
  return (
    <HeaderWrapper>
      <Link to="/">
        <Logo />
      </Link>
      <SignOut />
    </HeaderWrapper>
  );
};

export default Header;
