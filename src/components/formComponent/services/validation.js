import { findBySpecialRules, findRequiredField, findMaxQuantitySymbols, findForbiddenSymbols } from './helper';

const regEpx = {
  forbidSymbols: /[~<>]/,
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@_#\$%\^&\*])/,
  email: /(\w+@[a-zA-Z_]+?\.(com|ru|ua))/,
};

export const verifyEmail = (email, emailLength = 50) => {
  if (!email) {
    return findRequiredField('email');
  } else if (regEpx.forbidSymbols.test(email)) {
    return findForbiddenSymbols('email');
  } else if (email.length > emailLength) {
    return findMaxQuantitySymbols('email', emailLength);
  } else if (!regEpx.email.test(email)) {
    return findBySpecialRules('email', `Should be '@' sign and domain name '.com/.ru/.ua'`);
  }
};

export const verifyPassword = (password, passwordLength = 10) => {
  if (!password) {
    return findRequiredField('password');
  } else if (regEpx.forbidSymbols.test(password)) {
    return findForbiddenSymbols('password');
  } else if (password.length > passwordLength) {
    return findMaxQuantitySymbols('password', passwordLength);
  } else if (!regEpx.password.test(password)) {
    return findBySpecialRules('password', 'Lower/upper case letter, number and symbol');
  }
};

export const verifySearch = (search, searchLength = 100) => {
  if (regEpx.forbidSymbols.test(search)) {
    return findForbiddenSymbols('search');
  } else if (search.length > searchLength) {
    return findMaxQuantitySymbols('password', searchLength);
  }

  return search;
};
