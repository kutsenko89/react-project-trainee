import React from 'react';
import { Button } from '../button/button';
import Arrow from '../../../public/svg/Arrow.svg';
import { Input } from '../input/input';
import { PageWrapper } from './formComponentStyle';
import { useForm } from 'react-hook-form';
import { verifyPassword, verifyEmail } from './services/validation';
import PropTypes from 'prop-types';

export const FormComponent = ({ titleText, buttonText, inputContent, children, onSubmit, ...props }) => {
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm({ mode: 'onChange' });

  const renderInput = inputs => {
    return inputs.map(input => {
      return <Input key={input.labelText} {...input} register={register} errors={errors} />;
    });
  };

  const getSubmitData = inputField => {
    if ('email' in inputField) {
      const validateEmail = verifyEmail(inputField?.email);

      if (validateEmail?.error) {
        return setError(validateEmail.name, { ...validateEmail });
      }
    }

    if ('password' in inputField) {
      const validatePassword = verifyPassword(inputField?.password);

      if (validatePassword?.error) {
        return setError(validatePassword.name, { ...validatePassword });
      }
    }

    onSubmit(inputField);
  };

  return (
    <PageWrapper>
      <h1>{titleText}</h1>
      <form onSubmit={handleSubmit(getSubmitData)}>
        {renderInput(inputContent)}
        {children}
        <Button type="submit" {...props}>
          {buttonText}
          <svg>
            {' '}
            <Arrow />
          </svg>
        </Button>
      </form>
    </PageWrapper>
  );
};

FormComponent.propTypes = {
  titleText: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  inputContent: PropTypes.arrayOf(
    PropTypes.shape({
      inputType: PropTypes.string.isRequired,
      labelText: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
};
