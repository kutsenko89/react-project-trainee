import React from 'react';
import { Navigate } from 'react-router-dom';
import { getTokenStatus } from '../../helper';

export const PublicRoute = ({ children }) => {
  const token = getTokenStatus();

  return token ? <Navigate to="/" /> : children;
};
