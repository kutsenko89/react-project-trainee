import React from 'react';
import { getTokenStatus } from '../../helper';
import { Navigate } from 'react-router-dom';

export const PrivateRoute = ({ children }) => {
  const token = getTokenStatus();

  return !token ? <Navigate to="/sign-in" /> : children;
};
