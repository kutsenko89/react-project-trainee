import styled from 'styled-components';
import { theme } from '../../../theme/theme';

export const CourseProcessHeaderWrapper = styled.div`
  width: 100%;
  height: 272px;
  position: fixed;
  top: 0;
  left: 0;
  background: ${theme.colors.darkGrey};
  color: ${theme.colors.white};
  display: flex;
  align-items: flex-end;
  padding: 0 400px 24px 100px;
  justify-content: space-between;

  && .process-mainInfo__moveBack {
    background: none;
    border: none;
    cursor: pointer;
  }

  && .process-mainInfo svg {
    filter: invert();
    width: 18px;
    height: 18px;
    transform: rotate(180deg);
    margin: 11px;
  }

  && .process-mainInfo__name {
    font-size: 32px;
    color: ${theme.colors.white};
    display: flex;
    align-items: center;
    margin-bottom: 32px;
  }

  && .process-mainInfo__ranked {
    width: 36px;
    height: 36px;
    font-size: 16px;
    line-height: 18px;
    font-weight: bold;
    color: black;
    background: ${theme.colors.white};
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    visibility: ${({ ranked }) => !ranked && 'hidden'};
    margin-left: 8px;
  }

  && .process-technology {
    color: black;
    font-size: 20px;
    background: ${theme.colors.white};
    display: inline-block;
    padding: 12px;
    margin: 0 8px;
    border-radius: 6px;
    visibility: ${({ technologies }) => technologies?.some(item => !item) && 'hidden'};
  }

  && .process-mentor {
    display: flex;
    flex-direction: column;
    font-size: 16px;
    font-weight: bold;
  }

  && .process-mentor::before {
    content: 'Mentor:';
    font-weight: normal;
  }
`;
