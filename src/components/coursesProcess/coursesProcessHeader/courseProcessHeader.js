import React from 'react';
import { CourseProcessHeaderWrapper } from './courseProcessHeaderStyle';
import { combineMentorInfo } from '../../../services/serviceHelpers';
import Arrow from '../../../../public/svg/Arrow.svg';
import { useNavigate } from 'react-router-dom';

export const CourseProcessHeader = ({ technologies, name, ranked, mentor }) => {
  const navigate = useNavigate();

  return (
    <CourseProcessHeaderWrapper ranked={ranked} technologies={technologies}>
      <div className="process-mainInfo">
        <div className="process-mainInfo__name">
          <button className="process-mainInfo__moveBack" onClick={() => navigate(-1)}>
            <Arrow />
          </button>
          {name}
          <span className="process-mainInfo__ranked">{ranked}</span>
        </div>
        {technologies?.map(item => (
          <h4 className="process-technology " key={item}>
            {item}
          </h4>
        ))}
      </div>

      <div className="process-mentor">{combineMentorInfo(mentor)}</div>
    </CourseProcessHeaderWrapper>
  );
};
