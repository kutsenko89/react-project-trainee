import React from 'react';
import { CoursesProcessContentWrapper } from './coursesProcessContentStyle';
import { CoursesProcessList } from './coursesProcessList';
import { getDeadline } from '../courseProcessHelper';

export const CoursesProcessContent = ({ studyProcess }) => {
  const findActiveChapters = (chapters, process) => {
    if (process.status.state === 'Ended') {
      return 'is-done';
    }

    if (process.status.state === 'Not started') {
      return 'active';
    }

    if (chapters < process.activeChapter.order) {
      return 'is-done';
    }

    return 'active';
  };

  return (
    <CoursesProcessContentWrapper>
      {studyProcess?.chapters.map(chapter => {
        return (
          <CoursesProcessList
            key={chapter.id}
            id={chapter.id}
            chapter={chapter}
            studyProcess={studyProcess.admins}
            name={studyProcess?.courseName}
            chapterName={chapter.name}
            deadline={getDeadline(studyProcess)}
            ranked={studyProcess?.status.averageMark}
            marks={chapter?.marks}
            order={findActiveChapters(chapter.order, studyProcess)}
          />
        );
      })}
    </CoursesProcessContentWrapper>
  );
};
