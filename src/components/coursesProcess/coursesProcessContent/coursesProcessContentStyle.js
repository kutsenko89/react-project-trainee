import styled from 'styled-components';
import { theme } from '../../../theme/theme';

export const CoursesProcessContentWrapper = styled.ul`
  position: relative;
  padding-top: 24px;
  list-style: none;

  &::before {
    display: inline-block;
    content: '';
    position: absolute;
    top: 0;
    left: 15px;
    width: 10px;
    height: 100%;
  }

  & .item {
    position: relative;
    counter-increment: list;

    &:not(:last-child) {
      padding-bottom: 20px;
    }

    &::before {
      display: inline-block;
      content: '';
      position: absolute;
      height: 100%;
      width: 10px;
      border-left: 2px solid ${theme.colors.white};
      top: 25px;
      left: -83px;
    }

    &:last-child::before {
      border-left: none;
    }

    &::after {
      content: '';
      position: absolute;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      padding-top: 1px;
      width: 36px;
      height: 36px;
      top: 25px;
      left: -100px;
      font-size: 16px;
      text-align: center;
      color: ${theme.colors.darkGrey};
      border: none;
      background-color: ${theme.colors.white};
    }

    &.is-done {
      &::before {
        border-left: 2px solid ${theme.colors.darkGrey};
        top: 25px;
        left: -83px;
      }

      &:last-child::before {
        border-left: none;
      }

      &::after {
        content: '✔';
        display: flex;
        justify-content: center;
        align-items: center;
        padding-top: 1px;
        width: 36px;
        height: 36px;
        top: 25px;
        left: -100px;
        font-size: 16px;
        text-align: center;
        color: ${theme.colors.white};
        border: none;
        background-color: ${theme.colors.darkGrey};
      }
    }

    &.active {
      &:last-child::before {
        border-left: none;
      }

      &::before {
        border-left: 2px solid ${theme.colors.white};
        top: 25px;
        left: -83px;
      }

      &::after {
        content: counter(list);
        display: flex;
        justify-content: center;
        align-items: center;
        padding-top: 1px;
        width: 36px;
        height: 36px;
        top: 25px;
        left: -100px;
        font-size: 16px;
        text-align: center;
        color: ${theme.colors.darkGrey};
        border: none;
        background-color: ${theme.colors.white};
      }
    }
  }

  && .order {
    display: flex;
  }

  && .status {
    transform: translateX(-118px);
    border: 5px solid black;
  }

  && .container {
    width: 924px;
    min-height: 152px;
    background: ${theme.colors.white};
    border-radius: 12px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-start;
  }

  && .container-main {
    padding: 24px 24px 0 24px;
    width: 100%;
    display: flex;
    justify-content: space-between;
  }

  && .content-panel {
    height: 100%;
    align-self: center;
  }

  && .content-panel__name {
    font-size: 32px;
    color: ${theme.colors.darkGrey};
    font-weight: bold;
  }

  && .content-panel__chapter {
    margin-top: 8px;
    font-size: 16px;
    line-height: 16px;
    color: ${theme.colors.darkGrey};
    font-weight: normal;
  }

  && .content-panel__footer {
    width: 100%;
    padding: 0 24px 24px 24px;
  }

  && .content-panel__button {
    width: 18px;
    height: 18px;
    background: none;
    border: none;
    cursor: pointer;
    margin-top: 20px;
    transform: rotate(90deg);
    transition: 0.5s linear;
  }

  && .content-panel__button.active {
    transition: 0.5s linear;
    transform: rotate(-90deg);
  }

  && .content-info {
    justify-content: center;
    align-items: center;
    display: flex;
    font-size: 32px;
    color: ${theme.colors.gray};
    font-weight: bold;
  }

  && .content-info__deadline {
    font-size: 32px;
    color: ${theme.colors.darkGrey};
    font-weight: bold;
  }

  && .content-info__ranked {
    width: 36px;
    height: 36px;
    font-size: 16px;
    line-height: 18px;
    font-weight: bold;
    color: ${theme.colors.white};
    background: ${theme.colors.darkGrey};
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: 31px;
  }
`;
