import Arrow from '../../../../public/svg/Arrow.svg';
import React, { useState } from 'react';
import { CourseProcessComments } from '../coursesProcessComments/courseProcessComments';

export const CoursesProcessList = props => {
  const [openInfo, setOpenInfo] = useState(false);

  const handleOpenInfo = () => {
    setOpenInfo(prev => !prev);
  };

  return (
    <li className={`item ${props.order}`}>
      <div className="container">
        <div className="container-main">
          <div className="content-panel">
            <div className="content-panel__name">{props.name}</div>
            <div className="content-panel__chapter">{props.chapterName}</div>
          </div>
          <div className="content-info">
            Deadline: <span className="content-info__deadline">{props?.deadline}</span>
            <div className="content-info__ranked">{props?.ranked}</div>
          </div>
        </div>
        {
          <CourseProcessComments
            visible={openInfo ? 'visible' : 'hidden'}
            chapter={props.chapter}
            studyProcess={props.studyProcess}
            marks={props.marks}
          />
        }
        <div className="content-panel__footer">
          <button className={`content-panel__button ${openInfo && 'active'}`} onClick={handleOpenInfo}>
            {<Arrow />}
          </button>
        </div>
      </div>
    </li>
  );
};
