import styled from 'styled-components';

export const CourseProcessCommentsWrapper = styled.ul.attrs(props => ({ className: props.className }))`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 60px;
  overflow: hidden;
  &&.hidden {
    opacity: 0;
    max-height: 0;
    min-height: 0;
    transition: 0.5s linear;
  }

  &&.visible {
    opacity: 1;
    max-height: 1000px;
    visibility: visible;
    transition: 0.7s linear;
  }

  & li {
    border-top: 1px solid black;
    display: flex;
    justify-content: space-between;
    padding: 16px 24px;
  }

  & .comment-message-container {
    width: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
  }

  & li:last-child {
    border-bottom: 1px solid black;
  }

  & .comment-role {
    display: flex;
    flex-direction: column;
    align-self: center;
    width: 186px;
  }

  & .comment-role__name {
    font-size: 16px;
    font-weight: bold;
  }

  & .comment-message {
    display: flex;
    flex-direction: column;
  }

  & .comment-message__text {
    max-width: 450px;
    font-size: 16px;
    font-weight: bold;
    word-wrap: break-word;
    overflow: hidden;
  }

  & .comment-points {
    display: flex;
    flex-direction: column;
    padding-right: 144px;
  }

  & .comment-points__mark {
    font-size: 16px;
    font-weight: bold;
  }
`;
