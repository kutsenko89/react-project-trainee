import React from 'react';
import { CourseProcessCommentsWrapper } from './courseProcessCommentsStyle';
import { findCommentator, findMark } from '../courseProcessHelper';

export const CourseProcessComments = ({ chapter, studyProcess, marks, visible }) => {
  const renderCommentList = chapters => {
    return chapters.comments.map((chapter, index) => {
      const commentatorInfo = findCommentator(studyProcess, chapter);
      const markInfo = findMark(marks, chapter);

      return (
        <li key={`${chapter.userId + index}`}>
          <div className="comment-message-container">
            <div className="comment-role">
              {commentatorInfo?.studyAdminRole}
              <div className="comment-role__name">{commentatorInfo?.name}</div>
            </div>
            <div className="comment-message">
              Comment:
              <div className="comment-message__text">{chapter.comment}</div>
            </div>
          </div>
          <div className="comment-points">
            Points:
            <div className="comment-points__mark">{markInfo.mark}</div>
          </div>
        </li>
      );
    });
  };

  return (
    <CourseProcessCommentsWrapper className={visible} chapter={chapter.comments.length}>
      {renderCommentList(chapter)}{' '}
    </CourseProcessCommentsWrapper>
  );
};
