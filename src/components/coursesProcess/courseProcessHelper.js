export const findMark = (marks, chapter) => {
  return marks?.find(mark => mark?.initiatorId === chapter?.initiatorId) || {};
};

export const findCommentator = (studyProcess, chapter) => {
  return studyProcess?.find(role => role.accountId === chapter.initiatorId) || {};
};

export const getDeadline = studyProcess => {
  return studyProcess?.deadline.split('T', 1)[0].split('-').reverse().join('.');
};
