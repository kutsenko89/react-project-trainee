import React, { useEffect, useState } from 'react';
import { StudyCard } from './studyCard';
import { useSearchParams } from 'react-router-dom';

export const CardList = ({ courses }) => {
  const [filter, setFilter] = useState(false);
  const [params] = useSearchParams();

  const validateSearch = params.get('search') || '';

  useEffect(() => {
    if (validateSearch) {
      setFilter(true);
    }
  }, [validateSearch]);

  const renderCurrentCourses = (courses = []) => {
    return courses?.map(course => {
      return (
        <StudyCard
          key={course.id}
          courseId={course.id}
          name={course.name}
          technology={course.technologies}
          mentor={course.mentor}
          rating={course.rating}
          stateCourse={course.activeChapter}
        />
      );
    });
  };

  const filterCourses = courses => {
    return courses?.filter(item => {
      for (let key in item) {
        if (String(item[key]).toLowerCase().includes(validateSearch.toLowerCase())) {
          return item;
        }
      }
    });
  };

  return <>{filter ? renderCurrentCourses(filterCourses(courses)) : renderCurrentCourses(courses)}</>;
};
