import styled from 'styled-components';
import { theme } from '../../theme/theme';

export const StudyCardWrapper = styled.div`
  width: 1030px;
  height: 244px;
  margin: 16px;
  background: #ffffff;
  border-radius: 12px;
  align-self: center;

  @media (max-width: ${theme.screensWidth.desktop}) {
    width: 100%;
    max-width: 1030px;

    && .container {
      width: 100%;
      height: 100%;
    }
  }
  && .container {
    height: 100%;
    display: flex;
    flex-direction: column;
    align-content: space-between;
    justify-content: space-between;
  }

  && .headerContainer {
    display: flex;
    flex-direction: row;
    padding: 24px;
    justify-content: space-between;
    align-content: space-between;
  }

  && .header-container-primary {
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }

  && .header-container-primary h2 {
    color: ${theme.colors.darkGrey};
    font-size: 32px;
    line-height: 36px;
    margin-bottom: 8px;
  }

  && .header-container-primary p {
    color: ${theme.colors.darkGrey};
    font-size: 16px;
    line-height: 20px;
    margin-bottom: 16px;
  }

  && .technology {
    display: flex;
  }

  && .header-container-primary h4 {
    color: ${theme.colors.white};
    font-size: 20px;
    background: black;
    display: inline-block;
    padding: 12px;
    margin: 0 8px;
    border-radius: 6px;
    visibility: ${({ technology }) => !technology[0] && 'hidden'};
  }

  && .header-container-info {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    justify-content: space-between;
  }

  && .header-container-info__rating {
    width: 36px;
    height: 36px;
    font-size: 16px;
    line-height: 18px;
    font-weight: bold;
    color: ${theme.colors.white};
    background: black;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    visibility: ${({ rating }) => !rating && 'hidden'};
  }

  && .header-container-info__mentor {
    display: flex;
    flex-direction: column;
    font-size: 16px;
    font-weight: bold;
  }
  && .header-container-info__mentor::before {
    content: 'Mentor:';
    font-weight: normal;
  }

  && .footerContainer {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    width: 100%;
    height: 75px;
    border-top: 1px solid black;
  }

  && .footerContainer div {
    color: ${theme.colors.lightGray};
    font-size: 30px;
  }

  && .footerContainer div a {
    color: black;
    text-decoration: none;
    text-align: center;
    margin: 26px;
  }
  && .footerContainer_content {
    color: ${theme.colors.lightGray};
    font-size: 32px;
    line-height: 36px;
  }

  && .footerContainer div a svg {
    margin-left: 5px;
  }
`;
