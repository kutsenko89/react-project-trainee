import React from 'react';
import { StudyCardWrapper } from './studyCardStyle';
import { Link } from 'react-router-dom';
import Arrow from '../../../public/svg/Arrow.svg';

export const StudyCard = ({ name, technology, rating, mentor, stateCourse, courseId }) => {
  return (
    <StudyCardWrapper technology={technology} rating={rating}>
      <div className="container ">
        <div className="headerContainer">
          <div className="header-container-primary skeleton-container">
            <h2 className="skeleton skeleton-text">{name}</h2>
            <p className="skeleton skeleton-text">{name}</p>
            <div className="technology skeleton skeleton-technology">
              {technology.map(item => (
                <h4 className="skeleton " key={item}>
                  {item}
                </h4>
              ))}
            </div>
          </div>
          <div className="header-container-info skeleton-info">
            <div className="header-container-info__rating skeleton ">{rating}</div>
            <div className="header-container-info__mentor skeleton skeleton-text">{mentor}</div>
          </div>
        </div>
        <div className="footerContainer skeleton-container-footer">
          <div className="footerContainer_content skeleton skeleton-footer">
            {name && 'Next:'}
            <Link to={`/courses/:${courseId}`}>
              {stateCourse}
              <Arrow />
            </Link>
          </div>
        </div>
      </div>
    </StudyCardWrapper>
  );
};
