import styled from 'styled-components';
import { theme } from '../../theme/theme';

export const SignOutWrapper = styled.div`
  position: relative;
  width: 100px;
  height: 50px;
  color: ${theme.colors.white};
  cursor: pointer;
  font-size: 20px;
  &&.fade {
    visibility: hidden;
  }
  &&.visible {
    visibility: visible;
  }
`;
