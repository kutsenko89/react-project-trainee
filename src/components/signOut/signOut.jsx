import React, { useState } from 'react';
import { SignOutWrapper } from './signOutStyle';
import { useNavigate } from 'react-router-dom';

export const SignOut = () => {
  const navigate = useNavigate();
  const handleSignOut = () => {
    const token = localStorage.getItem('access_token');

    if (token) {
      localStorage.removeItem('access_token');
      navigate('/');
    }
  };

  return (
    <SignOutWrapper onClick={handleSignOut}>
      <span>Sign out</span>
    </SignOutWrapper>
  );
};
