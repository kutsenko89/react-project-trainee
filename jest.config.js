const config = {
  testEnvironment: 'jsdom',
  moduleNameMapper: { '\\.svg': '<rootDir>/__mocks__/svg.js' },
};

module.exports = config;
